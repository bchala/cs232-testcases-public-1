/*
    CHA does not realize that the only object type assigned to obj is B. 
    CHA only looks at declared types (in this case A) and its subclasses, therefore CHA does not de-virtualize obj.m(_) call. 
    Whereas 0CFA is able to detect that and de-virtualizes obj.m(_) to B_m(_).
    Similar to above, CHA does not realize that object passed as actual parameter to obj.m is only of type S and does not de-virtualize arg.p(_) method call.
    Again, 0CFA is able to detect this and is able to replace arg.p(_) in B_m with S_p(_).
    The nifty part is that 0CFA should be able to de-virtualize in 2 levels whereas CHA is not even capable of one! 
*/
class Example {
    public static void main(String[] a){
        A obj;
        obj = new B();
        System.out.println(obj.m(new S()));
    }
}

class A {
    public int m(Q arg){
        return arg.p(10);
    }
}

class B extends A {
    public int m(Q arg){
        return arg.p(5);
    }
}

class Q {
    public int p(int x) {
        return x*2;
    }
}

class S extends Q {
    public int p(int x) {
        return x*3;
    }
}