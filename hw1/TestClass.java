/*
** This factorial case tests the recursive function
** along with simple inheritance
** Updated on Feb 2
*/


class TestClass {
  public static void main(String[] a){
    System.out.println(new A().factorial(10));
  }
}

class A{
  public int factorial(int num) {
    int tmp;
    if (num < 1) {
      tmp = new B().initialValue();
    } else{
      tmp = num * (this.factorial(num - 1));
    }
    return tmp;
  }

  public int initialValue() {
    return 1;
  }
}

class B extends A{
  int varB;
  public int factorial(int num) {
    return num;
  }
}

class C extends A{
  int varC;
}
